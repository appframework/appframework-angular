/// <reference path="../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('appframework')
        .factory('Paging', Paging);

    Paging.$inject = ['$rootScope'];

    /**
     * @param {appframework.IAppframeworkRootScopeService} $rootScope
     */
    function Paging($rootScope) {
        return {
            pager: function() {
                return {
                    predicate: '',
                    reverse: false,

                    /** @type {ng.resource.IResourceClass | undefined} */
                    factory: undefined,
                    data: [],

                    total: 0,
                    pagesize: 20,
                    currentPage: 1,
                    maxPages: 20,

                    is_loading: false,

                    order: function(predicate) {
                        const vm = this;

                        vm.reverse = (predicate === predicate) ? !vm.reverse : false;
                        vm.predicate = predicate;
                        vm.loadData();
                    },

                    loadData: function() {
                        const vm = this;

                        vm.is_loading = true;
                        return vm.factory.query({'limit': vm.pagesize, 'offset': ((vm.currentPage - 1) * vm.pagesize), 'sortColumn': vm.predicate, 'ascending': !vm.reverse, 'search': $rootScope.search}, function(data) {
                            let total = (arguments[1]('Result-Count'));
                            if (!total) {
                                total = data.length;
                            }
                            vm.total = total;
                            vm.is_loading = false;
                            vm.data = data;
                            return data;
                        });
                    }
                };
            }
        };
    }

})();
