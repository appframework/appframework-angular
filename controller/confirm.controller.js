/// <reference path="../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    class ConfirmController {
        /**
         * @param {ng.ui.bootstrap.IModalInstanceService} $uibModalInstance
         * @param {string} entityname
         * @param {boolean} cascadeDeletes
         * @param {() => void} okCallback
         * @param {ng.translate.ITranslateService} $translate
         */
        constructor($uibModalInstance, entityname, cascadeDeletes, okCallback, $translate) {
            const vm = this;

            vm.entityname = entityname;
            vm.cascadeDeletes = cascadeDeletes;
            vm.ok = ok;
            vm.cancel = cancel;
            vm.confirmButtons = [{ value: 'ok', label: $translate.instant('remove') }];
            vm.cancelButtons = [{ value: 'abort', label: $translate.instant('abort') }];

            /**
             * @param {string} value
             */
            function ok(value) {
                $uibModalInstance.dismiss(value);
                okCallback();
            }

            /**
             * @param {string} value
             */
            function cancel(value) {
                $uibModalInstance.dismiss(value);
            }
        }
    }

    angular.module('appframework')
        .controller('ConfirmController', ConfirmController);

    ConfirmController.$inject = ['$uibModalInstance', 'entityname', 'cascadeDeletes', 'okCallback', '$translate'];

})();
