/// <reference path="../../../../../../node_modules/@types/angular/index.d.ts" />
/// <reference path="../../../../../../node_modules/@types/angular-cookies/index.d.ts" />
/// <reference path="appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular
        .module('appframework', ['ngRoute', 'ngResource', 'pascalprecht.translate', 'ngFileUpload', 'ui.bootstrap']);

    angular.module('appframework')
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider'];

    /**
     * @param {ng.route.IRouteProvider} $routeProvider
     */
    function config($routeProvider) {

        /**
         * @param {string} route
         * @param {string} templateUrl
         * @param {string} controller
         */
        function addRoute(route, templateUrl, controller) {
            $routeProvider.when(route, {
                templateUrl: templateUrl,
                controller: controller,
                controllerAs: 'vm'
            });
        }

        addRoute('/login', 'appframework/module/login/login.html', 'LoginController');
        addRoute('/profile', 'appframework/module/profile/profile.html', 'ProfileController');
        addRoute('/logout', 'appframework/module/login/logout.html', 'LogoutController');

        addRoute('/roles', 'appframework/module/role/roles.html', 'RoleListController');
        addRoute('/roles/:id', 'appframework/module/role/role.html', 'RoleDetailController');
        addRoute('/users', 'appframework/module/user/users.html', 'UserListController');
        addRoute('/users/:id', 'appframework/module/user/user.html', 'UserDetailController');
        addRoute('/users/clone/:id', 'appframework/module/user/user.html', 'UserDetailController');
    }

    run.$inject = ['$rootScope', '$location', '$http', '$window'];

    /**
     *
     * @param {appframework.IAppframeworkRootScopeService} $rootScope
     * @param {ng.ILocationService} $location
     * @param {ng.IHttpService} $http
     * @param {ng.IWindowService} $window
     */
    function run($rootScope, $location, $http, $window) {
        // keep user logged in after page refresh
        const storedState = $window.localStorage.getItem('state');
        if (storedState) {
            $rootScope.state = JSON.parse(storedState);
        } else {
            $rootScope.state = {user:{},permissions:[]};
        }
        if ($rootScope.state.user && $rootScope.state.user.authdata) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.state.user.authdata;
            $http.get('rest/permissions/my').then(function(response) {
                $rootScope.state.permissions = response.data;
                $window.localStorage.setItem('state', JSON.stringify($rootScope.state));
            }, function(error) {
                // If wrong user data was stored, remove it
                if (error && error.status === 401) {
                    delete $rootScope.state.user.authdata;
                    $window.localStorage.setItem('state', JSON.stringify($rootScope.state));
                }
            });
        }

        $rootScope.$on('$locationChangeStart', function() {
            // redirect to login page if not logged in
            if ($location.path() !== '/login' && (!$rootScope.state.user || !$rootScope.state.user.authdata)) {
                $location.path('/login');
            }
        });
    }

})();
