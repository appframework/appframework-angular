/// <reference path="../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('appframework')
        .directive('labelGroup', labelGroup);

    function labelGroup() {
        let autoincrementId = 0;

        /**
         * @param {af.ILabelGroupScope} scope
         * @param {JQLite} element
         */
        function link(scope, element) {
            const controls = element.find(':input');
            if (controls.length > 0) {
                let id = controls[0].id;
                if (!id) {
                    id = 'control_id_' + (autoincrementId++);
                    controls[0].id = id;
                }
                scope.forControl = id;
            }
        }

        return {
            templateUrl: 'appframework/directive/labelGroup.html',
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: {
                label: '@',
                valuecolumns: '@',
                labelcolumns: '@'
            },
            link: link
        };
    }
})();
