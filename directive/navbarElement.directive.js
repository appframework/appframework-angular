/// <reference path="../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('appframework')
        .directive('navbarElement', navbarElement);

    function navbarElement() {
        return {
            templateUrl: 'appframework/directive/navbarElement.html',
            restrict: 'E',
            replace: true,
            scope: {
                main: '=',
                permission: '@',
                module: '@',
                icon: '@',
                title: '@'
            }
        };
    }
})();
