// @ts-check

import {IRootScopeService} from "angular";
import "../../../../../../node_modules/@types/toastr/index.d.ts";
import "../../../../../../node_modules/@types/angular-resource";
import "../../../../../../node_modules/@types/angular-cookies/index.d.ts";
import "../../../../../../node_modules/@types/angular-ui-bootstrap/index.d.ts";

export as namespace appframework;
export = appframework;

declare namespace appframework {

    namespace dto {
        interface IResource<T> extends angular.resource.IResource<T> {
            $update(success: Function, error?: (response: ng.IHttpResponse<Problem>) => void): ng.IPromise<T>;
        }

        interface AbstractDto {
            id: number;
        }

        interface Role extends angular.resource.IResourceClass<Role>, IResource<Role>, AbstractDto {
            name: string;
            permissions: Permission[];
        }

        interface User extends angular.resource.IResourceClass<User>, IResource<User>, AbstractDto {
            name: string;
            login: string;
            password: string;
            email: string;
            roleIds: number[];
            pictureId: number;
        }

        interface BinaryData extends angular.resource.IResourceClass<BinaryData>, IResource<BinaryData>, AbstractDto {
            filename: string;
            size: number;
            contentType: string;
        }

        interface VersionInformation {
            maven: string;
            git: string;
            buildTimestamp: Date;
        }

        interface Permission extends String {}

        interface Problem {
            type: string;
            title: string;
            detail: string;
        }
    }

    namespace controller {
        interface LoginController extends ng.IScope {
            error?: string;
            username?: string;
            password?: string;
            login(): void;
        }

        interface RoleDetailController {
            isSelected(permission: dto.Permission): boolean;
            toggleSelection(permission: dto.Permission): void;
            save(): void;
            role: dto.Role;
            permissions: dto.Permission[];
        }

        interface RoleListController {
            paging: Pager<dto.Role>;
            add(): void;
            searchFilter(search: string): void;
            permissions: dto.Permission[];
            isSelected(permission: dto.Permission, role: dto.Role): boolean;
            remove(role: dto.Role): void;
        }

        interface UserDetailController {
            isSelected(roleId: number): boolean;
            toggleSelection(roleId: number): void;
            user: dto.User;
            roles: dto.Role[];
            passwordRepeated: string;
            save(): void;
        }

        interface UserListController {
            paging: Pager<dto.User>;
            roleLookup: dto.Role[];
            add(): void;
            clone(id: number): void;
            searchFilter(search: string): void;
            remove(user: dto.User): void;
        }

        interface ProfileController {
            passwordRepeated: string;
            profile: dto.User;
            file: string;
            update(): Promise<void>;
        }

    }

    interface IAppframeworkUser {
        authdata?: string;
        username?: string;
    }

    interface IAppframeworkState {
        user: IAppframeworkUser;
        permissions: string[];
    }

    interface IAppframeworkRootScopeService extends IRootScopeService {
        search: string;
        state: IAppframeworkState;
    }

    interface IButton {
        value: string;
        label: string;
    }

    interface Progress<T> {
        result: T | undefined;
        value: number;
        max: number;
        message: string;
        completed: boolean;
        success: boolean;
    }

    interface PagingFactory {
        pager<T>(): Pager<T>;
    }

    interface Pager<T> {
        predicate: string;
        reverse: boolean;
        factory: angular.resource.IResourceClass<T>;
        data: T[];
        total: number;
        pagesize: number;
        currentPage: number;
        maxPages: number;
        is_loading: boolean;

        order(predicate: string): void;
        loadData(): void;
    }

    interface ILabelGroupScope extends angular.IScope {
        forControl: string;
    }

    interface LoginResult {
        success: boolean;
        message?: string;
    }

    interface CallbackFn {
        (result: LoginResult): void;
    }

    interface AuthenticationService {
        clearCredentials(): void;
        checkLogin(username: string, password: string, callback: CallbackFn): void;
        sha1(str: string): string;
    }

}
