/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('appframework')
        .factory('Role', Role);

    /**
     * @param {ng.resource.IResourceService} $resource
     */
    function Role($resource) {
        return $resource('rest/role/:id', { id: '@id' }, {
            update: {
                method: 'PUT'
            }
        });
    }
})();
