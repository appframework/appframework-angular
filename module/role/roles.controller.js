/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    class RoleListController {
        /**
         * @param {ng.ILocationService} $location
         * @param {appframework.IAppframeworkRootScopeService} $rootScope
         * @param {ng.IHttpService} $http
         * @param {appframework.dto.Role} Role
         * @param {ng.ui.bootstrap.IModalService} $uibModal
         * @param {ng.route.IRouteService} $route
         * @param {ng.IScope} $scope
         * @param {appframework.PagingFactory} Paging
         * @param {ng.translate.ITranslateService} $translate
         */
        constructor($location, $rootScope, $http, Role, $uibModal, $route, $scope, Paging, $translate) {
            /** @type {appframework.controller.RoleListController} */
            // @ts-ignore
            const vm = this;

            vm.paging = Paging.pager();

            vm.add = add;
            vm.searchFilter = searchFilter;
            vm.isSelected = isSelected;
            vm.remove = remove;

            activate();

            function activate() {
                vm.paging.predicate = 'name';
                vm.paging.factory = Role;
                vm.paging.loadData();
                $scope.$watch('search', function() {
                    vm.paging.currentPage = 1;
                    vm.paging.loadData();
                });

                $http.get('rest/permissions').then(
                    /** @param {ng.IHttpResponse<appframework.dto.Permission[]>} response */
                    function(response) {
                        vm.permissions = response.data.sort(function(a, b){
                            const aArr = a.split('_');
                            const bArr = b.split('_');
                            const typeCompared = aArr[1].localeCompare(bArr[1]);
                            if (typeCompared === 0) {
                                return aArr[0].localeCompare(bArr[0]);
                            }
                            return typeCompared;
                        });
                    });
            }

            function add() {
                $location.path('/roles/new');
            }

            /**
             * @param {string} string
             */
            function searchFilter(string) {
                $rootScope.search = string;
            }

            /**
             * @param {appframework.dto.Permission} permission
             * @param {appframework.dto.Role} role
             */
            function isSelected(permission, role) {
                return role.permissions.indexOf(permission) !== -1;
            }

            /**
             * @param {appframework.dto.Role} role
             */
            function remove(role) {
                $http.get('rest/role/deletable/' + role.id).then(function(cascadeDeletes) {
                    $uibModal.open({
                        templateUrl: 'appframework/controller/confirm.html',
                        controller: 'ConfirmController',
                        controllerAs: 'vm',
                        resolve: {
                            entityname: function() { return role.name; },
                            cascadeDeletes: function() { return cascadeDeletes.data; },
                            okCallback: function() {
                                return function() {
                                    role.$delete(
                                        function() {
                                            $route.reload();
                                        },
                                        /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                                        function(response) {
                                            toastr.error(response.data.detail, $translate.instant('deletion_failed'), { timeOut: 0 });
                                        });
                                };
                            }
                        }
                    });
                });
            }

        }
    }

    angular.module('appframework')
        .controller('RoleListController', RoleListController);

    RoleListController.$inject = ['$location', '$rootScope', '$http', 'Role', '$uibModal', '$route', '$scope', 'Paging', '$translate'];

})();
