/// <reference path="../../appframework.d.ts" />
//@ts-check
(function() {
    'use strict';

    class RoleDetailController {
        /**
         * @param {ng.route.IRouteService} $route
         * @param {ng.ILocationService} $location
         * @param {ng.translate.ITranslateService} $translate
         * @param {ng.IHttpService} $http
         * @param {appframework.dto.Role} Role
         */
        constructor($route, $location, $translate, $http, Role) {
            /** @type {appframework.controller.RoleDetailController} */
            //@ts-ignore
            const vm = this;

            vm.isSelected = isSelected;
            vm.toggleSelection = toggleSelection;

            activate();

            function activate() {
                if (!$route.current) {
                    return;
                }

                if ($route.current.params.id === 'new') {
                    vm.role = new Role();
                    vm.role.permissions = [];
                    vm.save = function() {
                        vm.role.$save(function() {
                            $location.path('/roles');
                        },
                        /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                        function(response) {
                            toastr.error(response.data.detail, $translate.instant('create_failed'), { timeOut: 0 });
                        });
                    };
                } else {
                    Role.get({ id: $route.current.params.id }).$promise.then(
                        /**
                         * @param {appframework.dto.Role} role
                         */
                        function(role) {
                            vm.role = role;
                        });
                    vm.save = function() {
                        vm.role.$update(function() {
                            $location.path('/roles');
                        },
                        /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                        function(response) {
                            toastr.error(response.data.detail, $translate.instant('update_failed'), { timeOut: 0 });
                        });
                    };
                }

                $http.get('rest/permissions').then(
                    /**
                     * @param {ng.IHttpResponse<appframework.dto.Permission[]>} response
                     */
                    function(response) {
                        vm.permissions = response.data;
                        vm.permissions = response.data.sort(function(a, b){
                            const aArr = a.split('_');
                            const bArr = b.split('_');
                            const typeCompared = aArr[1].localeCompare(bArr[1]);
                            if (typeCompared === 0) {
                                return aArr[0].localeCompare(bArr[0]);
                            }
                            return typeCompared;
                        });
                    });
            }

            /**
             * @param {appframework.dto.Permission} permission
             */
            function isSelected(permission) {
                return vm.role && vm.role.permissions.indexOf(permission) !== -1;
            }

            /**
             * @param {appframework.dto.Permission} permission
             */
            function toggleSelection(permission) {
                const index = vm.role.permissions.indexOf(permission);
                if (index !== -1) {
                    vm.role.permissions.splice(index, 1);
                } else {
                    vm.role.permissions.push(permission);
                }
            }
        }
    }

    angular.module('appframework')
        .controller('RoleDetailController', RoleDetailController);

    RoleDetailController.$inject = ['$route', '$location', '$translate', '$http', 'Role'];

})();
