/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular
        .module('appframework')
        .filter('permissionName', permissionName);

    permissionName.$inject = ['$filter'];

    /**
     * @param {ng.IFilterService} $filter
     */
    function permissionName($filter) {
        /**
         * @param {string} input
         */
        const fn = function(input) {
            return $filter('translate')('PERMISSION_' + input);
        };

        return fn;
    }
})();
