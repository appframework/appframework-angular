/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    class ProfileController {
        /**
         * @param {ng.IHttpService} $http
         * @param {ng.translate.ITranslateService} $translate
         * @param {ng.angularFileUpload.IUploadService} Upload
         */
        constructor($http, $translate, Upload) {
            /** @type {appframework.controller.ProfileController} */
            // @ts-ignore
            const vm = this;

            vm.passwordRepeated = '';

            vm.update = update;

            activate();

            function activate() {
                $http.get('rest/profile').then(
                    /** @param {ng.IHttpResponse<appframework.dto.User>} response */
                    function(response) {
                        vm.profile = response.data;
                        if (vm.profile.pictureId !== null) {
                            vm.file = 'rest/binary_data/' + vm.profile.pictureId;
                        }
                    });
            }

            function update() {
                prepareDto();
                $http.put('rest/profile', vm.profile).then(
                    /** @param {ng.IHttpResponse<appframework.dto.User>} response */
                    function(response) {
                        postSave(response.data);
                    },
                    /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                    function(response) {
                        toastr.error(response.data.detail, $translate.instant('update_failed'), { timeOut: 0 });
                    });
            }

            function prepareDto() {
                if (vm.profile.password === '') {
                    vm.profile.password = null;
                }
            }

            /**
             * @param {appframework.dto.User} profile
             */
            function postSave(profile) {
                if (vm.file) {
                    if (angular.isObject(vm.file)) {
                        Upload.upload({ url: 'rest/profile/upload', data: { file: vm.file, filename: vm.file.name }, method: 'POST' }).then(function() {
                            toastr.clear();
                            toastr.success('', $translate.instant('profile_updated'), { timeOut: 5000, closeButton: true });
                        },
                        /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                        function(response) {
                            toastr.error(response.data.detail, $translate.instant('update_failed'), { timeOut: 0 });
                        });
                    } else {
                        toastr.clear();
                        toastr.success('', $translate.instant('profile_updated'), { timeOut: 5000, closeButton: true });
                    }
                } else if (profile.pictureId) {
                    $http.delete('rest/binary_data/' + profile.pictureId).then(function() {
                        toastr.clear();
                        toastr.success('', $translate.instant('profile_updated'), { timeOut: 5000, closeButton: true });
                    },
                    /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                    function(response) {
                        toastr.error(response.data.detail, $translate.instant('update_failed'), { timeOut: 0 });
                    });
                } else {
                    toastr.clear();
                    toastr.success('', $translate.instant('profile_updated'), { timeOut: 5000, closeButton: true });
                }
            }
        }
    }

    angular
        .module('appframework')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['$http', '$translate', 'Upload'];

})();