/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    class UserDetailController {
        /**
         * @param {ng.route.IRouteService} $route
         * @param {ng.ILocationService} $location
         * @param {ng.translate.ITranslateService} $translate
         * @param {appframework.dto.User} User
         * @param {appframework.dto.Role} Role
         * @param {appframework.AuthenticationService} AuthenticationService
         */
        constructor($route, $location, $translate, User, Role, AuthenticationService) {
            /** @type {appframework.controller.UserDetailController} */
            // @ts-ignore
            const vm = this;

            vm.passwordRepeated = '';

            vm.isSelected = isSelected;
            vm.toggleSelection = toggleSelection;

            activate();

            function activate() {
                Role.query(
                    /**
                     * @param {appframework.dto.Role[]} roles
                     */
                    function(roles) {
                        vm.roles = roles;
                    });
                // @ts-ignore
                if ($route.current.params.id == 'new') {
                    vm.user = new User();
                    vm.user.roleIds = [];
                    vm.save = function() {
                        prepareDto();
                        vm.user.$save(
                            function() {
                                $location.path('/users');
                            },
                            /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                            function(response) {
                                toastr.error(response.data.detail, $translate.instant('create_failed'), { timeOut: 0 });
                            });
                    };
                } else {
                    const isClone = $location.path().slice(0, '/users/clone'.length) === '/users/clone';
                    // @ts-ignore
                    User.get({ id: $route.current.params.id }).$promise.then(function(user) {
                        vm.user = user;
                        if (isClone) {
                            vm.user.name = 'Clone of ' + vm.user.name;
                            // @ts-ignore
                            vm.user.id = undefined;
                        }
                        if (!vm.user.roleIds) {
                            vm.user.roleIds = [];
                        }
                    });
                    if (isClone) {
                        vm.save = function() {
                            prepareDto();
                            vm.user.$save(function() {
                                $location.path('/users');
                            },
                            /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                            function(response) {
                                toastr.error(response.data.detail, $translate.instant('update_failed'), { timeOut: 0 });
                            });
                        };
                    } else {
                        vm.save = function() {
                            prepareDto();
                            // @ts-ignore
                            vm.user.$update(function() {
                                toastr.success('', $translate.instant('profile_updated'), { timeOut: 5000, closeButton: true });
                                $location.path('/users');
                            },
                            /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                            function(response) {
                                toastr.error(response.data.detail, $translate.instant('update_failed'), { timeOut: 0 });
                            });
                        };
                    }
                }
            }

            function prepareDto() {
                if (vm.user.password === '') {
                    // @ts-ignore
                    vm.user.password = null;
                }
                if (vm.user.password) {
                    vm.user.password = AuthenticationService.sha1(vm.user.password);
                }
            }

            /**
             * @param {number} roleId
             */
            function isSelected(roleId) {
                return vm.user && vm.user.roleIds.indexOf(roleId) !== -1;
            }

            /**
             * @param {number} roleId
             */
            function toggleSelection(roleId) {
                const index = vm.user.roleIds.indexOf(roleId);
                if (index !== -1) {
                    vm.user.roleIds.splice(index, 1);
                } else {
                    vm.user.roleIds.push(roleId);
                }
            }
        }
    }

    angular.module('appframework')
        .controller('UserDetailController', UserDetailController);

    UserDetailController.$inject = ['$route', '$location', '$translate', 'User', 'Role', 'AuthenticationService'];

})();
