/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    class UserListController {
        /**
         * @param {ng.ILocationService} $location
         * @param {appframework.IAppframeworkRootScopeService} $rootScope
         * @param {appframework.dto.User} User
         * @param {appframework.dto.Role} Role
         * @param {ng.ui.bootstrap.IModalService} $uibModal
         * @param {ng.route.IRouteService} $route
         * @param {ng.IScope} $scope
         * @param {appframework.PagingFactory} Paging
         * @param {ng.translate.ITranslateService} $translate
         * @param {ng.IHttpService} $http
         */
        constructor($location, $rootScope, User, Role, $uibModal, $route, $scope, Paging, $http, $translate) {
            /** @type {appframework.controller.UserListController} */
            const vm = this;

            vm.paging = Paging.pager();
            vm.roleLookup = [];

            vm.add = add;
            vm.clone = clone;
            vm.searchFilter = searchFilter;
            vm.remove = remove;

            activate();

            function activate() {
                vm.paging.predicate = 'name';
                vm.paging.factory = User;

                Role.query(
                    /**
                     * @param {appframework.dto.Role[]} roles
                     */
                    function(roles) {
                        angular.forEach(roles, function(element) {
                            vm.roleLookup[element.id] = element;
                        });
                        vm.paging.loadData();
                    });
                $scope.$watch('search', function() {
                    vm.paging.currentPage = 1;
                    vm.paging.loadData();
                });
            }

            function add() {
                $location.path('/users/new');
            }

            /**
             * @param {number} id
             */
            function clone(id) {
                $location.path('/users/clone/' + id);
            }

            /**
             * @param {string} string
             */
            function searchFilter(string) {
                $rootScope.search = string;
            }

            /**
             * @param {appframework.dto.User} user
             */
            function remove(user) {
                $http.get('rest/user/deletable/' + user.id).then(function(cascadeDeletes) {
                    $uibModal.open({
                        templateUrl: 'appframework/controller/confirm.html',
                        controller: 'ConfirmController',
                        controllerAs: 'vm',
                        resolve: {
                            entityname: function() { return user.name; },
                            cascadeDeletes: function() { return cascadeDeletes.data; },
                            okCallback: function() {
                                return function() {
                                    user.$delete(function() {
                                        $route.reload();
                                    },
                                    /** @param {ng.IHttpResponse<appframework.dto.Problem>} response */
                                    function(response) {
                                        toastr.error(response.data.detail, $translate.instant('deletion_failed'), { timeOut: 0 });
                                    });
                                };
                            }
                        }
                    });
                });
            }

        }
    }

    angular.module('appframework')
        .controller('UserListController', UserListController);

    UserListController.$inject = ['$location', '$rootScope', 'User', 'Role', '$uibModal', '$route', '$scope', 'Paging', '$http', '$translate'];

})();
