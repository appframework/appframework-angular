/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular.module('appframework')
        .factory('User', User);

    /**
     * @param {ng.resource.IResourceService} $resource
     */
    function User($resource) {
        return $resource('rest/user/:id', { id: '@id' }, {
            update: {
                method: 'PUT'
            }
        });
    }
})();
