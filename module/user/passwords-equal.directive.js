/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular
        .module('appframework')
        .directive('equal', equal);

    function equal() {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elm, attrs, ctrl) {
                scope.$watch(attrs.ngModel, function() {
                    validate();
                });

                attrs.$observe('equal', function() {
                    validate();
                });

                const validate = function() {
                    const ownValue = ctrl.$viewValue || '';
                    const compareValue = attrs.equal || '';
                    ctrl.$setValidity('equal', ownValue === compareValue);
                };
            }
        };
    }

})();

