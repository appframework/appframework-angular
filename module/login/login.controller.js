/// <reference path="../../appframework.d.ts" />
//@ts-check
(function() {
    'use strict';

    class LoginController {
    /**
         * @param {ng.ILocationService} $location
         * @param {appframework.AuthenticationService} AuthenticationService
         */
        constructor($location, AuthenticationService) {
            /** @type {appframework.controller.LoginController} */
            // @ts-ignore
            const vm = this;

            vm.error = '';
            vm.username = undefined;
            vm.password = undefined;

            vm.login = login;

            activate();

            function activate() {
                AuthenticationService.clearCredentials();
            }

            function login() {
                vm.error = '';
                // @ts-ignore
                AuthenticationService.checkLogin(vm.username, vm.password, function(response) {
                    if (response.success) {
                        $location.path('/');
                    } else {
                        vm.error = response.message;
                    }
                });
            }
        }
    }

    angular
        .module('appframework')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService'];

})();