/// <reference path="../../appframework.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular
        .module('appframework')
        .controller('LogoutController', LogoutController);

    LogoutController.$inject = ['$location', 'AuthenticationService', '$http'];

    /**
     * @param {ng.ILocationService} $location
     * @param {af.AuthenticationService} AuthenticationService
     * @param {ng.IHttpService} $http
     */
    function LogoutController($location, AuthenticationService, $http) {
        //var vm = this;

        activate();

        function activate() {
            $http.get('rest/login/logout').then(function() {
                AuthenticationService.clearCredentials();
                $location.path('/customers');
            });
        }

    }
})();