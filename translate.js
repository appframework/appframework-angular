/// <reference path="../../../../../../node_modules/@types/angular/index.d.ts" />
/// <reference path="../../../../../../node_modules/dayjs/index.d.ts" />
//@ts-check

(function() {
    'use strict';

    angular
        .module('appframework')
        .config(['$translateProvider', config]);

    /*
    angular
        .module('appframework')
        .factory('nonTranslatedTextFinder', function() {
            return {
                setLocale: function() {},
                getInterpolationIdentifier: function() { return 'nonTranslatedTextFinder'; },
                interpolate: function(string) { return '[[' + string + ']]'; }
            }
        });
    */

    /**
     * @param {ng.translate.ITranslateProvider} $translateProvider
     */
    function config($translateProvider) {
        $translateProvider
            .useSanitizeValueStrategy('escape')
            .useStaticFilesLoader({
                files: [{
                    prefix: 'appframework/i18n/',
                    suffix: '.json'
                },{
                    prefix: 'app/scripts/i18n/',
                    suffix: '.json'
                }]
            })
            //.useInterpolation('nonTranslatedTextFinder')
            .registerAvailableLanguageKeys(['en', 'de'], {
                'en_*': 'en',
                'de_*': 'de'
            })
            .fallbackLanguage('en')
            .determinePreferredLanguage();
        if (typeof dayjs !== 'undefined') {
            dayjs.locale($translateProvider.preferredLanguage());
        }
    }
})();
